/** @type {import('tailwindcss').Config} */

module.exports = {
  content: ["./pages/*.{js,jsx,ts,tsx}", "./components/**/*.{js,jsx,ts,tsx}"],
  theme: {
    colors: {
      "main-color": "#FFF",
      "second-color": "#21092f",
      "font-color": "#21092F",
      "light-font-color": "#8F8694",
      "error-color": "#FF5050",
    },
    extend: {},
  },
  plugins: [],
};
