export enum InputTypes {
  CVC = "cvc",
  NUMBER = "number",
  EXP_DATE = "expDate",
  CARD_HOLDER = "cardHolderName",
}

export const STANDARD_CARD_NUMBER_LENGTH = 16;
export const MAX_CARD_NUMBER_LENGTH = 16;
export const MAX_CARD_EXPIRE_DATE_LENGTH = 5;
export const EXPIRATION_DATE_VALUE_LENGTH = 5;
