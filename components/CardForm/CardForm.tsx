import { SubmitHandler, useForm } from "react-hook-form";
import { FormSchema, formSchema } from "./formSchema";
import { useState } from "react";
import { zodResolver } from "@hookform/resolvers/zod";
import {
  creditCardExpDateFormatter,
  creditCardNumberValidation,
} from "../utils";
import {
  EXPIRATION_DATE_VALUE_LENGTH,
  InputTypes,
  MAX_CARD_EXPIRE_DATE_LENGTH,
  MAX_CARD_NUMBER_LENGTH,
  STANDARD_CARD_NUMBER_LENGTH,
} from "../constants";
import { AmexImg, CashImg, MastercardImg, VisaImg } from "../Images";

const imgMap = {
  3: AmexImg,
  4: VisaImg,
  5: MastercardImg,
};

const getCardIcon = (key: keyof typeof imgMap) => {
  if (key) {
    const Icon = imgMap[key] ?? CashImg;

    return <Icon />;
  }
};

export default function CardForm({}) {
  const [cardFirstNum, setCardFirstNum] = useState<keyof typeof imgMap>();
  const isAmex = Number(cardFirstNum) === 3;

  const {
    reset,
    register,
    handleSubmit,
    setValue,
    setFocus,
    formState: { errors },
  } = useForm<FormSchema>({
    mode: "onBlur",
    resolver: zodResolver(formSchema({ cvc: isAmex ? 4 : 3 })),
  });

  const onSubmit: SubmitHandler<FormSchema> = (data) => {
    console.log(data);
    reset();
  };

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;

    switch (name) {
      case InputTypes.CARD_HOLDER:
        setValue(name, value.toUpperCase());
        break;
      case InputTypes.EXP_DATE:
        setValue(name, creditCardExpDateFormatter(value));

        if (value.length === EXPIRATION_DATE_VALUE_LENGTH) {
          setFocus(InputTypes.CVC);
        }
        break;
      case InputTypes.NUMBER:
        if (!cardFirstNum) {
          setCardFirstNum(value[0] as unknown as keyof typeof imgMap);
        }
        if (!value.length) {
          setCardFirstNum(undefined);
        }

        if (
          value.length === STANDARD_CARD_NUMBER_LENGTH &&
          creditCardNumberValidation(value)
        ) {
          setFocus(InputTypes.EXP_DATE);
        }
        break;
    }
  };

  const iconPlacement = errors?.cardHolderName?.message ? "144px" : "132px";

  return (
    <form
      onSubmit={handleSubmit(onSubmit)}
      className="mt-[50%] lg:mt-[111px] relative flex flex-col w-[327px] h-[348px] lg:w-[381x] lg:h-[372px] lg:ml-[140px] lg:mb-[90px]"
    >
      <label>Имя держателя карты</label>
      <input
        className="lg:w-[381px] min-h-[45px]"
        {...register("cardHolderName")}
        onChange={handleInputChange}
        placeholder="IVAN IVANOV"
      />
      <div className="mt-2 text-xs text-error-color leading-3">
        {errors?.cardHolderName?.message}
      </div>

      <label className="mt-[18px] lg:mt-[26px]">Номер карты</label>
      <input
        className="lg:w-[381px] min-h-[45px]"
        type="number"
        {...register("number")}
        placeholder="1234 5678 9123 0000"
        onChange={handleInputChange}
        maxLength={MAX_CARD_NUMBER_LENGTH}
      />
      <div className="mt-2 text-xs text-error-color leading-3">
        {errors?.number?.message}
      </div>

      <span className={`absolute top-[${iconPlacement}] right-[-50px]`}>
        {getCardIcon(cardFirstNum)}
      </span>

      <div className="flex w-full mt-[14px] mb-[28px] lg:mb-[40px] lg:mt-[26px]">
        <div className="mr-[11px] lg:mr-[20px]">
          <label className="block mb-[9px] lg:w-[150px]">Срок действия</label>
          <input
            className="w-[72px] lg:w-[92px]"
            {...register("expDate")}
            onChange={handleInputChange}
            placeholder="MM/YY"
            maxLength={MAX_CARD_EXPIRE_DATE_LENGTH}
          />
          <div className="mt-2 text-xs text-error-color leading-3">
            {errors?.expDate?.message}
          </div>
        </div>

        <div>
          <label className="block mb-[9px]">CVC</label>
          <input
            className="w-[155px] lg:w-[191px]"
            {...register("cvc")}
            type="number"
            placeholder={isAmex ? "1234" : "123"}
            maxLength={isAmex ? 4 : 3}
          />
          <div className="mt-2 text-xs text-error-color leading-3">
            {errors?.cvc?.message}
          </div>
        </div>
      </div>
      <button
        className="bg-second-color text-main-color w-[327px] min-h-[53px] rounded-lg lg:w-[381px]"
        aria-label="confirm button"
      >
        Pay
      </button>
    </form>
  );
}
