import { z } from "zod";
import { creditCardNumberValidation } from "../utils";

export type FormSchema = z.infer<ReturnType<typeof formSchema>>;

type Options = {
  cardHolderName: z.ZodEffects<z.ZodString, string, string>;
  number: z.ZodEffects<z.ZodString, string, string>;
  expDate: z.ZodString;
  cvc: number;
};
export const formSchema = (options?: Partial<Options>) =>
  z.object({
    cardHolderName: z
      .string()
      .min(2, "Имя слишком короткое")
      .max(25, "Максимальное число символов 25")
      .transform((value) => value.toUpperCase()),
    number: z
      .string()
      .min(16, "Минимальное количество символов - 16")
      .max(19, "Мaксимальное количество символов - 19")
      .refine(
        (value) => creditCardNumberValidation(value),
        "Номер карты введен неверно",
      ),
    expDate: z.string().min(5, "Введите дату корректно"),
    cvc: z
      .string()
      .min(
        options.cvc ?? 3,
        `Количество символов для данной карты - ${options.cvc ?? 3}`,
      )
      .max(4, `Количество символов для данной карты - ${options.cvc ?? 3}`),
  });
