import Image from "next/image";

export function AmexImg() {
  return <Image src="/amex.png" alt="amex" width="34" height="34" />;
}

export function MastercardImg() {
  return (
    <Image src="/mastercard.png" alt="mastercard" width="58" height="34" />
  );
}

export function VisaImg() {
  return <Image src="/visa.png" alt="visa" width="34" height="34" />;
}

export function CashImg() {
  return <Image src="/cash.png" alt="visa" width="34" height="34" />;
}
