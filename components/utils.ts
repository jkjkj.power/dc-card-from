export const creditCardExpDateFormatter = (value: string) =>
  value
    .replace(/^([1-9]\/|[2-9])$/g, "0$1/")
    .replace(/^(0[1-9]|1[0-2])$/g, "$1/")
    .replace(/^([0-1])([3-9])$/g, "0$1/$2")
    .replace(/^(0?[1-9]|1[0-2])([0-9]{2})$/g, "$1/$2")
    .replace(/^([0]+)\/|[0]+$/g, "0")
    .replace(/[^\d\/]|^[\/]*$/g, "")
    .replace(/\/\//g, "/");

export const creditCardNumberValidation = (value: string) => {
  let nCheck = 0;
  let bEven = false;

  value = value.replace(/\D/g, "");

  for (let n = value.length - 1; n >= 0; n--) {
    let cDigit = value.charAt(n);
    let nDigit = parseInt(cDigit, 10);

    if (bEven && (nDigit *= 2) > 9) nDigit -= 9;

    nCheck += nDigit;
    bEven = !bEven;
  }

  return nCheck % 10 === 0;
};
